package practica1_swing;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Yuluis
 */
public class ListaRopa extends Ropa implements Serializable {

    private ArrayList<Ropa> Prendas;

    public ListaRopa() {
        Prendas = new ArrayList<>();
    }

    public ArrayList<Ropa> getPrendas() {
        return Prendas;
    }

    public void setPrendas(ArrayList<Ropa> Prendas) {
        this.Prendas = Prendas;
    }

    public boolean existe(Ropa R) {
        return Prendas.contains(R);
    }

    public void altaPrenda(Ropa R) {
        Prendas.add(R);
    }
    
    public int medida(){
        return Prendas.size();
    }

    public ListaRopa ropaColor(String color) {
        ListaRopa ropaColor = new ListaRopa();
        for (Ropa R : Prendas) {
            if (color.equalsIgnoreCase(R.getColor())) {
                ropaColor.altaPrenda(R);
            }
        }
        return ropaColor;
    }
    
        public ListaRopa ropaTalla(String talla) {
        ListaRopa ropaTalla = new ListaRopa();
        for (Ropa R : Prendas) {
            if (talla.equalsIgnoreCase(R.getTalla())) {
                ropaTalla.altaPrenda(R);
            }
        }
        return ropaTalla;
    }
    
    public ListaRopa ropaColorTalla(String color, String talla){
        ListaRopa ropaColorTalla = new ListaRopa();
        for (Ropa R : Prendas){
            if (color.equalsIgnoreCase(R.getColor()) && talla.equalsIgnoreCase(R.getTalla())){
                ropaColorTalla.altaPrenda(R);
            }
        }
        return ropaColorTalla;
    }

    public int prendasTotal() {
        int numropa = 0;
        for (Ropa R : Prendas) {
            numropa += +R.getStock();
        }
        return numropa;
    }
    
    @Override
    public String toString() {
        return "ListaRopa{" + "Prendas=" + Prendas + '}';
    }
      
}
