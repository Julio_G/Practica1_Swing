package practica1_swing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Julio
 */
public class Ropa implements Serializable{

    private String codigo;
    private String descripcion;
    private String color;
    private String talla;
    private int stock;
    private double precio_coste;
    private double precio_venta;

    
    public Ropa() {
        codigo = "";
        descripcion = "";
        talla = "";
        color = "";

    }

    public Ropa(String codigo, String descripcion, String color, String talla, int stock, double precio_coste, double precio_venta) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.color = color;
        this.talla = talla;
        this.stock = stock;
        this.precio_coste = precio_coste;
        this.precio_venta = precio_venta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public double getPrecio_coste() {
        return precio_coste;
    }

    public void setPrecio_coste(double precio_coste) {
        this.precio_coste = precio_coste;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ropa other = (Ropa) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ropa{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", color=" + color + ", talla=" + talla + ", stock=" + stock + ", precio_coste=" + precio_coste + ", precio_venta=" + precio_venta + '}';
    }
    
    

}
